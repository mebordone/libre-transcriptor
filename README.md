# speech2text
Convert audio to text! Using https://github.com/alphacep/vosk-api


# Installation
1. Clone repo 
2. Install ffmpeg `apt-get install -y ffmpeg`
3. Install conda
4. Create environment: `conda env create -f environment.yml`
5. create folder transcriptions
6. Activate environment: `conda activate speech2text`

# Usage console
1. Download the file you want to transcribe
2. Transcribe: `python3 main.py your_file_to_transcribe.mp4`

# Usage GUI
1. Download the file you want to transcribe
2. use `python3 s2tgui.py`
3. lokfor the result in the "transcriptions" folder
 