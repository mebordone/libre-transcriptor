from tkinter import *
from tkinter.filedialog import askopenfile, asksaveasfilename
from getTranscript import GetTranscript
import sys
import os

ventana = Tk()
ventana.title("Transcripción Libre")
# ventana.iconbitmap("archivo.ico") # definir uno
frame=Frame()
frame.config(width=800, height=600)

#portada=PhotoImage(file="grafica.png")
#Label(frame,image=portada).grid(row=0,column=0)
#Label(frame,text="Seleccione el archivo que desea transcribir").grid(row=1,column=0)

path_archivo = StringVar()

def open_file():
    file = askopenfile(mode='r') # , filetypes=[('text files', '*.txt')]
    video_filepath = file.name
    file.close()
    video_filename = os.path.basename(video_filepath)
    path_archivo.set(video_filename)
    transcriptor = GetTranscript()
    resultado = transcriptor.process2text(video_filepath)
    transcript_texto.insert('1.0',resultado)


archivo_entrada_label = Label(frame,text="Abrir archivo")
archivo_entrada_label.grid(row=0,column=0, padx= 10, pady=10)
archivo_entrada_texto = Entry(frame, textvariable = path_archivo)
archivo_entrada_texto.grid(row=0,column=1, sticky="w", padx= 10, pady=10)
boton_abrir = Button(frame,text="Seleccionar", command=open_file)
boton_abrir.grid(row=0,column=3, sticky="w", padx= 10, pady=10)


transcript_label = Label(frame,text="Transcipción")
transcript_label.grid(row=1,column=0, padx= 10, pady=10)
transcript_texto = Text(frame,width=40,height=10)
transcript_texto.grid(row=1,column=1, padx= 10, pady=10)
scrollVert = Scrollbar(frame,command=transcript_texto.yview)
scrollVert.grid(row=1,column=2,sticky="nsew")
transcript_texto.config(yscrollcommand = scrollVert.set)


def save_file():
    file = asksaveasfilename(title="Save", filetypes=[('text files', '*.txt')])
    with open(file, "w") as data:
        data.write(transcript_texto.get("1.0", END))

boton_guardar = Button(frame,text="Guardar", command=save_file)
boton_guardar.grid(row=1,column=3, sticky="w", padx= 10, pady=10)

frame.pack()




ventana.mainloop()